﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBNF_ParserGen
{
    class Rule
    {
        public string Name;
        public string Desc;

        public List<string> StructMembers;
        public List<string> MatchCode;
        public List<string> ParseCode;

        public List<Rule> SubRules;

        public Rule()
        {
            Name = "";
            Desc = "";
            StructMembers = new List<string>();
            MatchCode = new List<string>();
            ParseCode = new List<string>();
            SubRules = new List<Rule>();

            MatchCode.Add("int tmp_idx = idx;");
            ParseCode.Add("int tmp_idx = idx;");
        }
    }

    class Program
    {
        enum TagKind
        {
            None,
            String,
            Reference,
            List,
            Group,
            Optional,
        }

        static string[] ParseConcats(string line)
        {
            int cur_lev = 0;
            var concats = new List<string>();
            string cur_piece = "";
            bool within_str = false;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] != ',' | within_str | cur_lev > 0)
                    cur_piece += line[i];

                if (line[i] == '\'')
                    within_str = !within_str;

                if (within_str)
                    continue;

                if (line[i] == '{' | line[i] == '[' | line[i] == '(')
                    cur_lev++;

                if (line[i] == '}' | line[i] == ']' | line[i] == ')')
                    cur_lev--;

                if (cur_lev == 0 && line[i] == ',')
                {
                    concats.Add(cur_piece);
                    cur_piece = "";
                }
            }

            concats.Add(cur_piece);

            return concats.ToArray();
        }

        static string FuncName(int lv_idx, int alt_idx, int top_i, int r_idx, int i, int j)
        {
            return $"_{r_idx}_{lv_idx}_{alt_idx}_{top_i}_{i}_{j}";
        }

        static string FuncName_IsPresent(int lv_idx, int alt_idx, int top_i, int r_idx, int i, int j)
        {
            return $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}_IsPresent";
        }

        static string FuncName_Parse(int lv_idx, int alt_idx, int top_i, int r_idx, int i, int j)
        {
            return $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}_Parse";
        }

        static string FuncName_Bool(int lv_idx, int alt_idx, int top_i, int r_idx, int i, int j)
        {
            return $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}_present";
        }

        static string UniqueIntName(int lv_idx, int alt_idx, int top_i, int r_idx, int i, int j)
        {
            return $"idx_{lv_idx}_{alt_idx}_{top_i}_{r_idx}_{i}_{j}";
        }

        static void ParseSubexp(string[] concats, Rule r, bool isOpt, bool isRep, int lv_idx, int alt_idx, int top_i, int r_idx)
        {
            if (lv_idx == 0)
            {
                r.ParseCode.Add($"{r.Name} u = new {r.Name}();");
            }

            r.MatchCode.Add("o_idx = idx;");
            r.MatchCode.Add("if(idx >= tkns.Length) return false;");

            int base_subrule_idx = 0;
            for (int i = 0; i < concats.Length; i++)
            {
                var alternates = concats[i].Split(new string[] { " | " }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Trim()).ToArray();
                var stringOptions = new string[alternates.Length];
                var tagKinds = new TagKind[alternates.Length];

                for (int j = 0; j < alternates.Length; j++)
                    if (alternates[j].StartsWith("'") && alternates[j].EndsWith("'"))
                    {
                        tagKinds[j] = TagKind.String;
                        stringOptions[j] = alternates[j].Substring(1, alternates[j].Length - 2);
                        r.SubRules.Add(null);
                    }
                    else if (alternates[j].StartsWith("[") && alternates[j].EndsWith("]"))
                    {
                        tagKinds[j] = TagKind.Optional;
                        var conc_parts = ParseConcats(alternates[j].Substring(1, alternates[j].Length - 2));
                        Rule r_sub = new Rule();
                        r_sub.Name = $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}";
                        ParseSubexp(conc_parts, r_sub, true, false, lv_idx + 1, j, i, r_idx);
                        r.SubRules.Add(r_sub);
                    }
                    else if (alternates[j].StartsWith("{") && alternates[j].EndsWith("}"))
                    {
                        tagKinds[j] = TagKind.List;
                        var conc_parts = ParseConcats(alternates[j].Substring(1, alternates[j].Length - 2));
                        Rule r_sub = new Rule();
                        r_sub.Name = $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}";
                        ParseSubexp(conc_parts, r_sub, false, true, lv_idx + 1, j, i, r_idx);
                        r.SubRules.Add(r_sub);
                    }
                    else if (alternates[j].StartsWith("(") && alternates[j].EndsWith(")"))
                    {
                        tagKinds[j] = TagKind.Group;
                        var conc_parts = ParseConcats(alternates[j].Substring(1, alternates[j].Length - 2));
                        Rule r_sub = new Rule();
                        r_sub.Name = $"{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}";
                        ParseSubexp(conc_parts, r_sub, false, false, lv_idx + 1, j, i, r_idx);
                        r.SubRules.Add(r_sub);
                    }
                    else
                    {
                        tagKinds[j] = TagKind.Reference;
                        r.SubRules.Add(null);
                    }
                stringOptions = stringOptions.Distinct().Where(a => !string.IsNullOrEmpty(a)).ToArray();

                if (tagKinds.Any(a => a == TagKind.String))
                {
                    //Add a string member
                    if (isRep)
                    {
                        r.StructMembers.Add($"public List<string> strVal_{lv_idx}_{alt_idx}_{i};");
                        r.ParseCode.Add($"u.strVal_{lv_idx}_{alt_idx}_{i} = new List<string>();");
                    }
                    else if (stringOptions.Length > 1)
                        r.StructMembers.Add($"public string strVal_{lv_idx}_{alt_idx}_{i};");

                    string optionVals = $"var strValOpts_{lv_idx}_{alt_idx}_{i} = new string[] {{";
                    for (int j = 0; j < stringOptions.Length; j++)
                        optionVals += "\"" + stringOptions[j] + "\", ";
                    optionVals += "};";

                    r.MatchCode.Add(optionVals);
                    r.ParseCode.Add(optionVals);

                    if (isRep)
                        r.MatchCode.Add($"while( strValOpts_{lv_idx}_{alt_idx}_{i}.Contains(tkns[idx].TokenValue) ) {{ idx++; tmp_idx++; }}");
                    else
                    {
                        r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, 0)}_str = strValOpts_{lv_idx}_{alt_idx}_{i}.Contains(tkns[idx].TokenValue); idx++; tmp_idx++;");
                        r.MatchCode.Add($"if (!{FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, 0)}_str) return false;");
                    }

                    if (stringOptions.Length > 1)
                        r.ParseCode.Add($"u.strVal_{lv_idx}_{alt_idx}_{i} = tkns[idx].TokenValue; idx++; tmp_idx++;");
                    else
                        r.ParseCode.Add($"idx++; tmp_idx++;");
                }

                for (int j = 0; j < alternates.Length; j++)
                {
                    switch (tagKinds[j])
                    {
                        case TagKind.Reference:
                            {
                                //Add the specific reference member
                                if (isRep)
                                    r.StructMembers.Add($"public List<{alternates[j]}> {FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)} = new List<{alternates[j]}>();");
                                else
                                    r.StructMembers.Add($"public {alternates[j]} {FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)};");

                                if (alternates.Length > 1)
                                    r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} = {alternates[j]}_IsPresent(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)});");
                                else
                                {
                                    r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} = {alternates[j]}_IsPresent(tkns, idx, out tmp_idx); idx = tmp_idx;");
                                    r.MatchCode.Add($"if (!{FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)}) return false;");
                                }

                                if (isRep)
                                    r.ParseCode.Add($"u.{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)}.Add({alternates[j]}_Parse(tkns, idx, out tmp_idx)); idx = tmp_idx;");
                                else if (alternates.Length > 1)
                                    r.ParseCode.Add($"if ({alternates[j]}_IsPresent(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)})) {{ u.{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)} = {alternates[j]}_Parse(tkns, idx, out tmp_idx); idx = tmp_idx; }}");
                                else
                                    r.ParseCode.Add($"u.{FuncName(lv_idx, alt_idx, top_i, r_idx, i, j)} = {alternates[j]}_Parse(tkns, idx, out tmp_idx); idx = tmp_idx;");
                            }
                            break;
                        case TagKind.List:
                            {
                                if (alternates.Length > 1)
                                    throw new Exception("Syntax Error");

                                r.StructMembers.AddRange(r.SubRules[base_subrule_idx + j].StructMembers);
                                r.MatchCode.Add($"while({FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out tmp_idx)) idx = tmp_idx;");
                                r.ParseCode.Add($"while({FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)})) {{ {FuncName_Parse(lv_idx, alt_idx, top_i, r_idx, i, j)}(u, tkns, idx, out tmp_idx); idx = tmp_idx; }}");
                            }
                            break;
                        case TagKind.Optional:
                            {
                                if (alternates.Length > 1)
                                    throw new Exception("Syntax Error");

                                //Extract the optional reference
                                r.StructMembers.AddRange(r.SubRules[base_subrule_idx + j].StructMembers);
                                r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} = {FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out tmp_idx); if({FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)}) idx = tmp_idx;");
                                r.ParseCode.Add($"if ({FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)})) {{ {FuncName_Parse(lv_idx, alt_idx, top_i, r_idx, i, j)}(u, tkns, idx, out tmp_idx); idx = tmp_idx; }}");
                            }
                            break;
                        case TagKind.Group:
                            {
                                //Extract the name of the list reference
                                r.StructMembers.AddRange(r.SubRules[base_subrule_idx + j].StructMembers);

                                if (alternates.Length > 1)
                                    r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} = {FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)});");
                                else
                                {
                                    r.MatchCode.Add($"bool {FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} = {FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out tmp_idx); idx = tmp_idx;");
                                    r.MatchCode.Add($"if (!{FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)}) return false;");
                                }

                                if (alternates.Length > 1)
                                    r.ParseCode.Add($"if ({FuncName_IsPresent(lv_idx, alt_idx, top_i, r_idx, i, j)}(tkns, idx, out int {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)})) {{ {FuncName_Parse(lv_idx, alt_idx, top_i, r_idx, i, j)}(u, tkns, idx, out tmp_idx); idx = tmp_idx; }}");
                                else
                                    r.ParseCode.Add($"{FuncName_Parse(lv_idx, alt_idx, top_i, r_idx, i, j)}(u, tkns, idx, out tmp_idx); idx = tmp_idx;");
                            }
                            break;
                    }

                    if (tagKinds[j] != TagKind.String && j < alternates.Length - 1 && alternates.Length > 0 && tagKinds[j + 1] != TagKind.String)
                        r.ParseCode.Add("else");
                }

                string tagOr = "";
                for (int j = 0; j < alternates.Length; j++)
                    switch (tagKinds[j])
                    {
                        case TagKind.Reference:
                        case TagKind.List:
                        case TagKind.Group:
                            tagOr += $"!{FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)} && ";
                            break;
                    }
                if (tagOr.Length > 0 && alternates.Length > 1 && !isRep)
                {
                    tagOr = tagOr.Substring(0, tagOr.Length - 4);
                    r.MatchCode.Add($"if ({tagOr}) return false;");
                    for (int j = 0; j < alternates.Length; j++)
                    {
                        switch (tagKinds[j])
                        {
                            case TagKind.Reference:
                            case TagKind.List:
                            case TagKind.Group:
                                r.MatchCode.Add($"if ({FuncName_Bool(lv_idx, alt_idx, top_i, r_idx, i, j)}) idx = {UniqueIntName(lv_idx, alt_idx, top_i, r_idx, i, j)};");
                                r.MatchCode.Add("else");
                                break;
                        }
                    }
                    r.MatchCode.RemoveAt(r.MatchCode.Count - 1);
                }
                base_subrule_idx += alternates.Length;
            }

            r.MatchCode.Add("o_idx = idx; return true;");
            r.ParseCode.Add("o_idx = idx;");
            if (lv_idx == 0)
                r.ParseCode.Add("return u;");
        }

        static List<string> code;

        static void SaveRule(Rule r, string path, int lv, Rule topLevel)
        {
            var p_code = new List<string>();
            var m_code = new List<string>();
            var s_code = new List<string>();

            m_code.Add($"internal static bool {r.Name}_IsPresent(Token[] tkns, int idx, out int o_idx) {{");
            if (lv == 0)
            {
                p_code.Add($"internal static {topLevel.Name} {r.Name}_Parse(Token[] tkns, int idx, out int o_idx) {{");
                s_code.Add($"internal class {topLevel.Name} {{");
            }
            else
            {
                p_code.Add($"internal static void {r.Name}_Parse({topLevel.Name} u, Token[] tkns, int idx, out int o_idx) {{");
            }

            p_code.AddRange(r.ParseCode);
            m_code.AddRange(r.MatchCode);
            if (lv == 0) s_code.AddRange(r.StructMembers);

            p_code.Add("}");
            m_code.Add("}");
            if (lv == 0) s_code.Add("}");

            code.AddRange(p_code);
            code.AddRange(m_code);
            if (lv == 0) code.AddRange(s_code);

            File.WriteAllLines($"{path}parse.cs", p_code.ToArray());
            File.WriteAllLines($"{path}match.cs", m_code.ToArray());
            File.WriteAllLines($"{path}struct.cs", s_code.ToArray());

            for (int i = 0; i < r.SubRules.Count; i++)
                if (r.SubRules[i] != null)
                    SaveRule(r.SubRules[i], path + $"{i}_", lv + 1, topLevel);
        }

        static Rule ParseLine(string line, int r_idx)
        {
            line = line.Trim();
            line = line.Substring(0, line.Length - 1);

            var name = line.Substring(0, line.IndexOf('=')).Trim();
            var desc = line.Substring(line.IndexOf('=') + 1).Trim();

            var r = new Rule()
            {
                Name = name,
                Desc = desc
            };

            var concats = ParseConcats(desc);
            ParseSubexp(concats, r, false, false, 0, 0, 0, r_idx);

            //Generate function templates and names
            SaveRule(r, $"{r_idx}_", 0, r);

            return r;
        }

        static void Main(string[] args)
        {
            var lines = File.ReadAllLines(@"I:\Code\VisualStudio\CardinalSemiCompiler\CardinalSemi_simplified.ebnf");

            code = new List<string>();
            code.Add("using System;");
            code.Add("using System.Linq;");
            code.Add("using System.Collections.Generic;");
            code.Add("namespace CardinalSemiCompiler {");
            code.Add("partial class CardinalSemiParser {");

            List<Rule> Rules = new List<Rule>();
            for (int i = 0; i < lines.Length; i++)
                if (!string.IsNullOrEmpty(lines[i].Trim()))
                {
                    var r = ParseLine(lines[i], i);
                    Rules.Add(r);
                }

            code.Add("}");
            code.Add("}");
            File.WriteAllLines("code.cs", code);
        }
    }
}
