﻿using CardinalSemiCompiler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompilerDriver
{
    class Program
    {
        /*static int tabCnt = 0;
        static void PrintSyntaxNode(SyntaxNode node)
        {
            for (int i = 0; i < tabCnt; i++)
                Console.Write("\t");

            Console.WriteLine(node);

            tabCnt++;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                PrintSyntaxNode(node.ChildNodes[i]);
            tabCnt--;
        }*/

        static void Compile(string programText)
        {
            Tokenizer tknizer = new Tokenizer(programText);
            var tkns = tknizer.Tokenize();
            for (int i = 0; i < tkns.Length; i++)
                Console.WriteLine(tkns[i]);

            //Build the syntax tree
            //SyntaxNode rNode = SyntaxTree.Build(tkns);

            //Print the syntax tree
            //Console.WriteLine("\n");
            //PrintSyntaxNode(rNode);
        }

        static void Main(string[] args)
        {
            const string programText =
@"using System.Main;
using System.Collections;
using System.Linq;
using System.Text;

class Test.Program : System.IDisposable
{
    void Main(string[] args)
    {
        if(args.Length >= 1) Console.WriteLine(""\tHello, World!"");
    }
}";

            string codePath = @"I:\Code\VisualStudio\CardinalSemiCompiler\CardinalSemiCompiler";
            var code = Directory.GetFiles(codePath, "*.cs", SearchOption.AllDirectories);

            Driver d = new Driver();
            /*for(int i = 0; i < code.Length; i++)
            {
                if (code[i].Contains("\\obj\\"))
                    continue;

                if (!code[i].Contains("Token.cs"))
                    continue;

                Console.WriteLine($"\nCompiling: {Path.GetFileName(code[i])}\n");
                d.Compile(File.ReadAllText(code[i]));
            }*/

            //CodeGenerator gen = new CodeGenerator();
            d.Compile(programText);

            Console.ReadLine();
        }
    }
}
