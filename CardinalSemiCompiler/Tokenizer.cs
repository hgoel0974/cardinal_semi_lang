﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardinalSemiCompiler
{
    public class Tokenizer
    {
        private string code;
        private int pos = 0;
        private int column = 0;
        private int line = 0;

        public Tokenizer(string code)
        {
            this.code = code;
        }

        private char? Eat()
        {
            if (pos < code.Length)
            {
                column++;
                return code[pos++];
            }
            return null;
        }

        private void Rollback()
        {
            column--;
            pos--;
        }

        private void EatWhitespace()
        {
            while (pos < code.Length && Syntax.IsWhitespace(code[pos]))
            {
                if (code[pos] == Syntax.Newline)
                {
                    line++;
                    column = 0;
                }
                pos++;
            }
        }

        private char? Peek(int cnt)
        {
            if ((pos + cnt) < code.Length)
                return code[pos + cnt];
            return null;
        }

        private string EatWhile(Func<char, bool> cond)
        {
            string s = "";
            char? c = code[pos - 1];
            do
            {
                s += c;
                c = Eat();
            } while (c != null && cond(c.Value));
            if (c != null) Rollback();
            return s;
        }

        private Token Process()
        {
            EatWhitespace();

            var l_p = pos;
            var l_l = line;
            var l_c = column;

            var _char_n = Eat();
            if (_char_n == null) return null;
            var _char = _char_n.Value;

            var _char0_n = Peek(0);
            if (_char0_n != null)
            {
                string _cmnt_blk = _char.ToString() + _char0_n.ToString();
                if (_cmnt_blk == Syntax.LineCommentStart)
                    while (Eat() != Syntax.Newline) ;
                else if (_cmnt_blk == Syntax.BlockCommentStart)
                {
                    var m_char = Eat();
                    while (m_char?.ToString() + Peek(0)?.ToString() != Syntax.BlockCommentEnd)
                        m_char = Eat();
                }
            }

            if (Syntax.IsSymbol(_char))
            {
                if (Syntax.MultipartSymbols.Contains(_char))
                {
                    var str = EatWhile(Syntax.IsSymbol);
                    return new Token(TokenType.Symbol, str, l_p, l_l, l_c);
                }
                else
                    return new Token(TokenType.Symbol, _char.ToString(), l_p, l_l, l_c);
            }
            else if (Syntax.IsPreprocessorStart(_char))
            {
                var str = EatWhile((c) => c != Syntax.Newline);
                return new Token(TokenType.Preprocessor, str, l_p, l_l, l_c);
            }
            else if (Syntax.IsNumber(_char))
            {
                var str = EatWhile(Syntax.IsNumber);
                return new Token(TokenType.Number, str, l_p, l_l, l_c);
            }
            else if (Syntax.IsCharStart(_char))
            {
                string charStr = "";

                char? _char_str_n = null;
                do
                {
                    if (_char_str_n != null)
                        charStr += _char_str_n.Value;

                    _char_str_n = Eat();
                    if (_char_str_n == null) throw new TokenException(ErrorMessages.UnexpectedEOF);
                    if (_char_str_n == Syntax.EscapeChar)
                    {
                        _char_str_n = Eat();
                        if (_char_str_n == null)
                            throw new TokenException(ErrorMessages.UnexpectedEOF);

                        switch (_char_str_n)
                        {
                            case 'u':
                                {
                                    if (!ushort.TryParse(EatWhile(Syntax.IsNumber), out var num))
                                        throw new TokenException(ErrorMessages.InvalidEscapeSequence);

                                    charStr += (char)num;
                                }
                                break;
                            case 'r':
                                charStr += '\r';
                                break;
                            case 'n':
                                charStr += '\n';
                                break;
                            case 'b':
                                charStr += '\b';
                                break;
                            case 't':
                                charStr += '\t';
                                break;
                            case '\'':
                                charStr += '\'';
                                break;
                            default:
                                {
                                    if (_char_str_n == Syntax.EscapeChar)
                                    {
                                        charStr += Syntax.EscapeChar;
                                        break;
                                    }
                                    throw new TokenException(ErrorMessages.InvalidEscapeSequence);
                                }
                        }
                        _char_str_n = Eat();
                    }
                } while (!Syntax.IsCharStop(_char_str_n.Value));
                return new Token(TokenType.Char, charStr, l_p, l_l, l_c);
            }
            else if (Syntax.IsStringStart(_char))
            {
                string charStr = "";

                char? _char_str_n = null;
                do
                {
                    if (_char_str_n != null)
                        charStr += _char_str_n.Value;

                    _char_str_n = Eat();
                    if (_char_str_n == null) throw new TokenException(ErrorMessages.UnexpectedEOF);
                    if (_char_str_n == Syntax.EscapeChar)
                    {
                        _char_str_n = Eat();
                        if (_char_str_n == null)
                            throw new TokenException(ErrorMessages.UnexpectedEOF);

                        switch (_char_str_n)
                        {
                            case 'u':
                                {
                                    if (!ushort.TryParse(EatWhile(Syntax.IsNumber), out var num))
                                        throw new TokenException(ErrorMessages.InvalidEscapeSequence);

                                    charStr += (char)num;
                                }
                                break;
                            case 'r':
                                charStr += '\r';
                                break;
                            case 'n':
                                charStr += '\n';
                                break;
                            case 'b':
                                charStr += '\b';
                                break;
                            case 't':
                                charStr += '\t';
                                break;
                            case '"':
                                charStr += '"';
                                break;
                            default:
                                {
                                    if (_char_str_n == Syntax.EscapeChar)
                                    {
                                        charStr += Syntax.EscapeChar;
                                        break;
                                    }
                                    throw new TokenException(ErrorMessages.InvalidEscapeSequence);
                                }
                        }
                        _char_str_n = Eat();
                    }
                } while (!Syntax.IsStringStop(_char_str_n.Value));
                return new Token(TokenType.String, charStr, l_p, l_l, l_c);
            }
            else if (Syntax.IsStartOfKeywordOrIdent(_char))
            {
                var kwd_ident_n = EatWhile(Syntax.IsKeywordOrIdent);
                if (Syntax.Keywords.Contains(kwd_ident_n))
                    return new Token(TokenType.Keyword, kwd_ident_n, l_p, l_l, l_c);
                else
                    return new Token(TokenType.Identifier, kwd_ident_n, l_p, l_l, l_c);
            }
            else if (Syntax.IsTerminator(_char))
            {
                return new Token(TokenType.Terminator, _char.ToString(), l_p, l_l, l_c);
            }

            return null;
        }

        public Token[] Tokenize()
        {
            List<Token> tkns = new List<Token>();
            Token t = null;
            do
            {
                t = Process();
                if (t != null) tkns.Add(t);
            } while (t != null);
            return tkns.ToArray();
        }
    }
}
