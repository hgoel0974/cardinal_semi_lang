﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardinalSemiCompiler
{
    class ErrorMessages
    {
        public const string UnexpectedEOF = "Unexpected end of file.";
        public const string InvalidEscapeSequence = "Invalid escape sequence.";
    }
}
