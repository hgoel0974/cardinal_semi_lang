﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardinalSemiCompiler
{
    public class Driver
    {
        public void Compile(string programText)
        {
            Tokenizer tknizer = new Tokenizer(programText);
            var tkns = tknizer.Tokenize();
            for (int i = 0; i < tkns.Length; i++)
                Console.WriteLine(tkns[i]);
            
            var topNode = CardinalSemiParser.TopLevel_Parse(tkns, 0, out int o_idx);
            //Build the syntax tree
            //SyntaxNode rNode = SyntaxTree.Build(tkns);

            //Print the syntax tree
            //Console.WriteLine("\n");
            //PrintSyntaxNode(rNode);
        }
    }
}
