﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardinalSemiCompiler
{
    public enum TokenType
    {
        Unknown,
        Number,
        Symbol,
        Terminator,
        Keyword,
        Identifier,
        Char,
        String,
        Preprocessor
    }

    public class Syntax
    {
        #region Token Definitions
        public static readonly char Newline = '\n';
        public static readonly char EscapeChar = '\\';

        public static readonly string LineCommentStart = "//";
        public static readonly string BlockCommentStart = "/*";
        public static readonly string BlockCommentEnd = "*/";

        public static readonly char[] MultipartSymbols = new char[] { '<', '>', '=', '|', '/', '-', '+', '*', '&', '^', '%', '!' };

        public static readonly string[] Keywords = new string[]
        {
            "abstract",
            "as",

            "base",
            "bool",
            "break",
            "byte",

            "case",
            "catch",
            "char",
            "checked",
            "class",
            "const",
            "continue",

            "decimal",
            "default",
            "delegate",
            "do",
            "double",

            "else",
            "enum",
            "event",
            "explicit",
            "extern",

            "false",
            "finally",
            "fixed",
            "float",
            "for",
            "foreach",

            "goto",
            "get",

            "if",
            "implicit",
            "in",
            "int",
            "interface",
            "internal",
            "is",

            "lock",
            "long",

            "namespace",
            "new",
            "null",
            "nameof",

            "object",
            "operator",
            "out",
            "override",

            "params",
            "private",
            "protected",
            "public",
            "partial",

            "readonly",
            "ref",
            "return",

            "sbyte",
            "sealed",
            "set",
            "short",
            "sizeof",
            "stackalloc",
            "static",
            "string",
            "struct",
            "switch",
            "this",
            "throw",
            "true",
            "try",
            "typeof",

            "uint",
            "ulong",
            "unchecked",
            "unsafe",
            "ushort",
            "using",

            "var",
            "virtual",
            "void",
            "volatile",
            "value",

            "while",
            "where",

            "yield",
        };

        private static readonly string[] PreprocessorTokens = new string[]
        {
            "#region",
            "#endregion",
            "#define",
            "#if",
            "#else",
            "#elif",
            "#endif",
            "#error",
            "#line",
        };
        #endregion

        public static bool IsWhitespace(char c)
        {
            return char.IsWhiteSpace(c);
        }

        public static bool IsNumber(char c)
        {
            return char.IsDigit(c);
        }

        public static bool IsSymbol(char c)
        {
            var chars = new char[] { '<', '>', '=', '(', ')', '[', ']', '{', '}', '|', '/', '-', '+', '*', '&', '^', '%', '$', '!', '~', '?', '.', ',', ':' };
            return chars.Contains(c);
        }

        public static bool IsTerminator(char c)
        {
            return c == ';';
        }

        public static bool IsPreprocessorStart(char c)
        {
            return c == '#';
        }

        #region Keywords and Identifiers
        public static bool IsStartOfKeywordOrIdent(char c)
        {
            var chars = new char[] { '_', '@' };
            return char.IsLetter(c) | chars.Contains(c);
        }

        public static bool IsKeywordOrIdent(char c)
        {
            return char.IsLetterOrDigit(c);
        }
        #endregion

        #region Char Delimiters
        public static bool IsCharStart(char c)
        {
            return c == '\'';
        }

        public static bool IsCharStop(char c)
        {
            return c == '\'';
        }
        #endregion

        #region String Delimiters
        public static bool IsStringStart(char c)
        {
            return c == '"';
        }

        public static bool IsStringStop(char c)
        {
            return c == '"';
        }
        #endregion
    }
}
