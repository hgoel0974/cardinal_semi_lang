﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardinalSemiCompiler
{
    partial class CardinalSemiParser
    {
        public class Identifier
        {
            public string Value;
        }

        public class CSNumber
        {
            public string Value;
        }

        public class CSString
        {
            public string Value;
        }

        public class CSChar
        {
            public string Value;
        }

        static bool Identifier_IsPresent(Token[] tkns, int idx, out int o_idx)
        {
            if (tkns[idx].TokenType == TokenType.Identifier)
            {
                o_idx = idx + 1;
                return true;
            }

            o_idx = idx;
            return false;
        }

        static Identifier Identifier_Parse(Token[] tkns, int idx, out int o_idx)
        {
            o_idx = idx + 1;
            return new Identifier()
            {
                Value = tkns[idx].TokenValue
            };
        }

        static bool CSNumber_IsPresent(Token[] tkns, int idx, out int o_idx)
        {
            if (tkns[idx].TokenType == TokenType.Number)
            {
                o_idx = idx + 1;
                return true;
            }

            o_idx = idx;
            return false;
        }

        static CSNumber CSNumber_Parse(Token[] tkns, int idx, out int o_idx)
        {
            o_idx = idx + 1;
            return new CSNumber()
            {
                Value = tkns[idx].TokenValue
            };
        }

        static bool CSString_IsPresent(Token[] tkns, int idx, out int o_idx)
        {
            if (tkns[idx].TokenType == TokenType.String)
            {
                o_idx = idx + 1;
                return true;
            }

            o_idx = idx;
            return false;
        }

        static CSString CSString_Parse(Token[] tkns, int idx, out int o_idx)
        {
            o_idx = idx + 1;
            return new CSString()
            {
                Value = tkns[idx].TokenValue
            };
        }

        static bool CSChar_IsPresent(Token[] tkns, int idx, out int o_idx)
        {
            if (tkns[idx].TokenType == TokenType.Char)
            {
                o_idx = idx + 1;
                return true;
            }

            o_idx = idx;
            return false;
        }

        static CSChar CSChar_Parse(Token[] tkns, int idx, out int o_idx)
        {
            o_idx = idx + 1;
            return new CSChar()
            {
                Value = tkns[idx].TokenValue
            };
        }
    }
}
